import re
import requests
import os


def filter_lines(file):
    for line in file.readlines():
        if line.startswith("---"):
            continue
        if line.startswith("-") or line.startswith("    -"):
            yield split_parts(line)


def split_parts(line):
    indented = line.startswith("    ")
    match = re.match(r".*\[(.*)\]\(\.(.*)\.md\)", line)
    return (indented, match.group(1), finish_url(match.group(2)))


def finish_url(tail):
    return f"https://dcacademy.gitlab.io/tutorials{tail}"


def webhook_edit(message):
    print(message)
    r = requests.patch(os.getenv("TUTORIAL_WEBHOOK"), json=message)
    print(r)


def embed_factory(lines):
    _, name, url = lines.__next__()
    embed = {"title": name, "url": url, "description": ""}
    for indented, name, url in lines:
        if indented:
            embed["description"] += f"* [{name}]({url})\n"
        else:
            yield embed
            embed = {"title": name, "url": url, "description": ""}
    yield embed


with open("./src/SUMMARY.md") as file:
    embeds = {"content": "**A list of our tutorials**", "embeds": []}
    for embed in embed_factory(filter_lines(file)):
        embeds["embeds"].append(embed)
    webhook_edit(embeds)
