# Command Arguments

You have now played around with commands and the Context argument but that doesn't allow for a very good interaction between the user and the bot. For that we need more arguments, just like a command like you do when executing a Python script, for example. `discord.py` incorporates an easy to learn argument system. All you have to do to get started is to specify arguments in the command function definition.   

Like this:  
```python
@bot.command(name="foo")
async def example_command(ctx, bar, baz):
    ...
```

That will generate a command which takes two arguments when invoked, which will be passed onto the command function as strings and you'll be able to access them just like a completly normal function. So, you could run it like this `!foo asdf movies` (supposing the bot prefix is `!`) and `bar` would be "asdf" and `baz` would be "movies".  

> **Note:** By default, `discord.py` raises a `MissingRequiredArguments` when you dont' pass values for all required arguments when invoking some command. However, it does not raise the `TooManyArguments` exception in case you pass to many arguments. To enable that you need to set the `ingore_extra` parameter to `False` in each command's decorator generator you want that behaviour to occur.

You should also learn about the behaviour `discord.py` gives `*`. In a "normal" Python context, this char serves as a boundary between the positional/keyword from the keyword-only arguments. `discord.py` uses it to mark whichever argument comes after it as the "rest argument", meaning that it will take whatever input is left over after parsing the other arguments. 
Here's an example to make this clearer:
```python
@bot.command(name="foo")
async def example_command(ctx, bar, baz, *, bork):
    ...
```
In this case, I could invoke the command like this `!foo asdf movies are great` which would result in `bar` being "asdf", `baz` being "movies" and `bork` being "are great".

You can make optional arguments by passing a default value, just like you'd do in the usual context.

There's more to command arguments though. `discord.py` comes packed with a cool argument converter system that allows you to easily cast the user input into whichever type you want.  

## Argument converters

Converters are a powerful and useful tool `discord.py` provides in its command framework. It allows you to automagically cast the user input into the types you need for your command logic. There are two ways you can use the conversion system:

1. Type annotate the argument with a function that takes the inputted string and returns some other object.
2. Create a class that derives from `commands.Converter` and override the `convert` method, which should take a context and the argument in question and return the converted result. You can then type annotate an argument with either just the class or an object of that class, if you want to pass some kind of configuration in the class constructor.

#### Method #1

Let's see two examples to illustrate this method.  

First, here's a very simple command that takes an integer `amount` and then sends `amount` snake emojis.  
*Note that this example is really simple and crude, not taking into consideration message char limits.*
```python
@bot.command(name="snakes")
async def spit_snakes_command(ctx, amount: int):
    await ctx.send(":snake: " * amount)
```
Typing `!snakes 5` would make the bot reply with `🐍 🐍 🐍 🐍 🐍 `. As you can see, I didn't need to cast `amount` into an int because the converter did it for me.

Now let's see another example with a function that automatically makes the user input ready for comparison using `str.casefold`.
```python
# Note, the type annotations in this function are not required
def casefold_arg(arg: str) -> str:
    return arg.casefold()

MAGIC_WORD = "bork"

@bot.command(name="magicword")
async def magic_word_command(ctx, word: casefold_arg):
    if word == MAGIC_WORD:
        reply = "Woohoo! You guessed my magic word! :D"
    else:
        reply = "Boooo! Get good mate."
    
    await ctx.send(reply)
```
If I typed `!magicword bOrK` it would reply with the success message. In this case, the argument was automatically `casefold`'d by the conversion system.

#### Method #2

This other way of using this system is to write converter classes, which is a good way to make argument converters with options. To do this you need to create a class that derives from `commands.Converter` and overwrite the async `convert` method, which will take the command context and the argument in question as arguments. You can also give it a constructor (`__init__`) to make it process some kind of options.

I'm going to make a really simple int converter that can take a custom base. Going to use the previous snake command example as a base.
```python
class IntConverterWithBases(commands.Converter):
    def __init__(self, base: int):
        if not isinstance(base, int):
            raise TypeError("Base must be an integer")

        self.base = base

    async def convert(self, ctx, argument):
        # Going to let a possible ValueError through so it can be caught later on
        return int(argument, self.base)

@bot.command(name="binarysnakes")
async def spit_snakes_command(ctx, amount: IntConverterWithBases(base=2)):
    await ctx.send(":computer " + ":snake: " * amount)
```
Invoking with `!binarysnakes 101` would make the bot reply with `💻 🐍 🐍 🐍 🐍 🐍 `, while `!binarysnakes 42` would make it error, since 42 isn't base 2.

`discord.py` comes with some of these classes for Discord models already packed and they are even accessible just by type annotating an argument with the Discord object, like a text channel for example. You can see the full list of built-in converters [**here**](https://rapptz.github.io/discord.py/docs/ext/commands/commands.html#discord-converters).

---

And we got to the end of this section! Try playing around with this mechanic for a bit and then head over to [**Checks and Cooldowns**](./checks-cooldowns.html). I'll be waiting for you there!


