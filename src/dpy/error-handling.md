# Error Handling

Error handling is an essential part of any bot or program really. `discord.py` relies on the built-in `logging` module to report everything from errors to a success message when connecting. To see the logs on your console you must enable it first. All you have to do is set the base configuration for logging to have some level (I recommend sticking with `INFO` or `DEBUG` if you need to suss out any internal issue with the library).  

This is how you can do it:
```python
# Add this module to your imports
import logging

...

# Add this line before the bot as it may spit out some stuff
logging.basicConfig(level=logging.INFO)

bot = ...

```
You should now be getting logs in your console. Hooray!

Now, this isn't really error handling, it's more like actually seeing errors :P   
`discord.py` offers two events that you can listen to in order to process exceptions that occur while the bot is running - `on_error` and `on_command_error`. The former gets triggered everytime any exception is raised, while the latter covers exceptions raised in a command invokation (only availbe in the Bot class).

Here's an example for those two events:
```python
# Very useless thing but this is an example ;)
exception_counter = 0

@bot.listen("on_error")
async def count_exceptions_on_error(error):
    exception_counter += 1


@bot.listen("on_command_error")
async def warn_on_command_cooldown(ctx, error):
    # Unwrapping the error cause because of how discord.py raises some of them
    error = error.__cause__ or error

    if isinstance(error, commands.CommandOnCooldown):
        await ctx.send(
            f"That command is on cooldown for `{error.retry_after:,.0f}` more seconds!"
            )
```

Alternatively, you can register an error handler directly to a command, like this:
```python
@bot.command(name="foo")
async def foo_command(ctx):
    ...

@foo_command.error
async def on_error(ctx, error):  # the function doens't actually need to be named 'on_error'
    ...
```
In this case that would only trigger for exceptions raised in that command's invokation.

---

We're done in this section! Hopefully you should be able to improve your bot with error handlers, and then proceed to the last main section of this tutorial - [**Extensions**](./extensions.html) - where I'll go over how you can split your bot's code into smaller, more manageable pieces.
